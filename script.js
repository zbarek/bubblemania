// canvas setup
const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');

canvas.width = 800;
canvas.height = 600;

let defaultFont = '50px Georga';
let score = 0;
let oxygen = 100;
let gameFrame = 0;
let gamePause = false;
let gameError = '';
let level = 1;

let background = new Image();
background.src = 'sprites/bg.jpg';


ctx.font = defaultFont;

window.addEventListener('resize', () => {
    window.location.reload();
})

// sounds
class GameSoundsRegistry {

    constructor() {
        this.data = {}
    }

    addSound(file, alias) {

        if (!alias) {
            alias = file;
        }

        alias = this.safeAliasName(alias);
        const sound = document.createElement('audio')
        sound.src = file;
        this.data[alias] = sound;

    }

    getSound(alias) {
        alias = this.safeAliasName(alias);
        if (this.data[alias] != 'undefined') {
            // console.log('Sound found:', alias)
            return this.data[alias];
        }
        console.log('Sound not found:', alias)
        return null;
    }

    safeAliasName(string) {
        return string.split("/").join("_").split("\\").join("_").split(".").join('_');
    }
}

let gameSounds = new GameSoundsRegistry();
gameSounds.addSound('sounds/bubbles-single1.wav', 'bubble1');
gameSounds.addSound('sounds/bubbles-single2.wav', 'bubble2');


// Mosue interactivity
const mouse = {
    x: canvas.width / 2,
    y: canvas.height / 2,
    click: false
}

let canvasPosition = canvas.getBoundingClientRect();

canvas.addEventListener('mousedown', function (event) {
    mouse.click = true;
    mouse.x = event.x - canvasPosition.left;
    mouse.y = event.y - canvasPosition.top;
    // console.log('mosue x,y: ', mouse.x, mouse.y);
});


canvas.addEventListener('mouseup', function (event) {
    mouse.click = false;
})
// Player

class Player {

    actions = ['swim right', 'swim left'];

    constructor() {
        this.x = canvas.width / 2;
        this.y = canvas.height / 2;
        this.radius = 20;
        this.angle = 0;
        this.frameX = 0;
        this.frameY = 0;

        this.maxFrame = 3;
        this.startFrame = 0;
        this.frame = this.startFrame;

        this.spriteWidth = 418;
        this.spritHeight = 397;
        this.oxygen = 100;
        this.imageLeft = new Image();
        this.imageLeft.src = 'sprites/player/player_swim.png';

        this.imageRight = new Image();
        this.imageRight.src = 'sprites/player/player_swim_right.png';

    }


    update() {
        const dx = this.x - mouse.x;
        const dy = this.y - mouse.y;
        let theta = 0;
        if (mouse.x <= this.x) {
            theta = Math.atan2(dy, dx);
        } else {
            theta = Math.atan2(-dy, - dx);
        }


        this.angle = theta;
        this.animationSpeed = 5 - Math.floor((dx / 20) + (dy / 20));

        if (mouse.x != this.x) {
            this.x -= dx / 20;
        }


        if (mouse.y != this.y) {
            this.y -= dy / 20;
        }



        if (this.oxygen < 0) {
            gamePause = true;
            alert('You died!');
            window.location.reload();
        }

        if (gameFrame % 100 === 0) {
            this.oxygen = this.oxygen - 2;
        }

        this.action = 'swim left';

        if (mouse.x <= this.x) {
            this.action = 'swim left';
        } else {
            this.action = 'swim right';
        }




    }


    draw() {
        if (mouse.click) {
            ctx.lineWidth = 0.2;
            ctx.beginPath();

            ctx.moveTo(this.x, this.y);
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = 'navy';
            ctx.stroke();
        }

        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();

        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.angle);

        if (this.action === 'swim left') {
            this.moveLeft()
        } else {
            this.moveRight()
        }

        ctx.restore();

    }

    moveRight() {

        // console.log(Math.ceil(this.x), Math.ceil(mouse.x))
        ctx.drawImage(this.imageRight, this.frameX * this.spriteWidth, 0, this.spriteWidth, this.spritHeight, - 33, - 30, this.spriteWidth / 7, this.spritHeight / 7);
        if (Math.abs(Math.ceil(this.x) - Math.ceil(mouse.x)) < 30) {
            this.frameX = this.maxFrame;
            this.onIdle();
            return;
        }


        this.onMove();
        if (gameFrame % this.animationSpeed === 0) {
            ++this.frameX;
            if (this.frameX > this.maxFrame) {
                this.frameX = this.startFrame;
            }
        }
    }

    moveLeft() {

        ctx.drawImage(this.imageLeft, this.frameX * this.spriteWidth, 0, this.spriteWidth, this.spritHeight, - 28, - 30, this.spriteWidth / 7, this.spritHeight / 7);
        // console.log(Math.ceil(this.x), Math.ceil(mouse.x))
        if (Math.abs(Math.ceil(this.x) - Math.ceil(mouse.x)) < 30) {
            this.frameX = this.startFrame;
            this.onIdle();
            return;
        }


        this.onMove();

        if (gameFrame % this.animationSpeed === 0) {
            ++this.frameX;
            if (this.frameX > this.maxFrame) {
                this.frameX = this.startFrame;
            }
        }
    }


    onMove() {
        setTimeout(() => {
            if (gameFrame % 15 === 0) {
                this.oxygen = this.oxygen - 2;
            }
        }, 50);

        if (gameFrame % 5 === 0) {


            if (this.y > canvas.height - 80) {
                const color = '#000';
                generateParticleCloud(this.x, this.y, color, 4, 10, -0.09);
            } else {
                generateParticleCloud(this.x, this.y, 'white', 10, 13, -0.3);
            }
        }
    }


    onIdle() {
        this.action = 'idle'
    }
}


const player = new Player();

// Bubbles
const bubblesArray = [];
const bubbleExplosionArray = [];

class Bubble {
    constructor() {
        this.x = Math.random() * canvas.width;
        this.y = canvas.height + Math.random() * canvas.height;
        this.radious = Math.random() * 40 + 10;
        this.speed = Math.random() * 5 + 1;
        this.distance = 0;
        this.color = '#41c0ff';
        this.counted = false;
        this.popSound = Math.random() < 0.5 ? gameSounds.getSound('bubble1') : gameSounds.getSound('bubble2');
        this.transparancy = 0.8;
        this.image = new Image();
        this.image.src = 'sprites/bubble/bubble.png';
        this.spriteWidth = 512;
        this.spriteHeight = 512;
    }

    setRadius(r) {
        this.radious = r;
    }
    setX(x) {
        this.x = x;
    }

    setY(y) {
        this.y = y;
    }

    update() {
        this.y -= this.speed;

        const dx = this.x - player.x;
        const dy = this.y - player.y;
        this.distance = Math.sqrt(dx * dx + dy * dy);

    }

    draw() {

        ctx.fillStyle = this.color;
        ctx.globalAlpha = this.transparancy;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radious, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();
        ctx.strokeStyle = 'white';
        ctx.stroke();


        ctx.drawImage(this.image, 0, 0, this.spriteWidth - 3, this.spriteHeight - 4, this.x - this.radious - 4, this.y - this.radious - 4, this.radious * 2 + 7, this.radious * 2 + 7);
        ctx.globalAlpha = 1;



    }
}


function bubbleExplosion(bubble, size, radius) {
    for (let i = 0; i < size; i++) {
        let b = new Bubble();
        b.setRadius(Math.random() * radius)
        b.setY(bubble.y);
        b.color = '#41c0ff'
        b.transparancy = 0.2;
        b.speed = b.speed + 2;
        if (Math.random() < 0.5) {
            b.setX(bubble.x - Math.random() * 20);
        } else {
            b.setX(bubble.x + Math.random() * 20);
        }


        bubbleExplosionArray.push(b);
    }


}

function handleBubbles() {
    if (gameFrame % (level * 50) === 0) {
        bubblesArray.push(new Bubble());
    }

    // bubble to pick
    for (let i = 0; i < bubblesArray.length; i++) {
        bubblesArray[i].update();
        bubblesArray[i].draw();

        if (bubblesArray[i].distance < bubblesArray[i].radious + player.radius) {

            if (!bubblesArray[i].counted) {
                bubbleExplosion(bubblesArray[i], Math.random() * 10, bubblesArray[i].radious / 2);
                bubblesArray[i].popSound.play();
                score++;
                bubblesArray[i].counted = true;

                //player.radius = player.radius + bubblesArray[i].radious * 0.2;


                player.oxygen = Math.ceil(player.oxygen + bubblesArray[i].radious * 0.5);

                if (player.oxygen > 100) {
                    player.oxygen = 100;
                }
                bubblesArray.splice(i, 1);
            }
        }


        if (bubblesArray[i]) {
            if (bubblesArray[i].y < 0 - bubblesArray[i].radious / 2) {
                bubblesArray.splice(i, 1);
            }
        }

    }

    // bubble explosions
    for (let j = 0; j < bubbleExplosionArray.length; j++) {
        bubbleExplosionArray[j].update();
        bubbleExplosionArray[j].draw();

        if (bubbleExplosionArray[j].y < 0 - bubbleExplosionArray[j].radious / 2) {
            bubbleExplosionArray.splice(j, 1);
        }
    }

    // particles
    for (let k = 0; k < partilclesArray.length; k++) {
        partilclesArray[k].update();
        partilclesArray[k].draw();

        if (partilclesArray[k].remove === true) {
            partilclesArray.splice(k, 1);
        }


    }
}

//  Sand particles
let partilclesArray = [];
class Particle {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.radius = Math.random() * 10;
        this.color = 'white';
        this.transparency = 0.2;
        this.remove = false;
        this.speed = - Math.random() * 0.2 - 0.9;
        this.lifeTime = Math.random() * 5000;
        this.removeAfter = new Date().getTime() + this.lifeTime;
    }

    update() {

        this.y = this.y + this.speed;
        // this.speed = this.speed + 0.1;

        if (this.y + this.radius > canvas.height) {
            this.y = canvas.height - this.radius;
            this.remove = true;
        }

        if (this.y < 0 || this.isTimeToRemove()) {
            this.remove = true;
        }
    }

    isTimeToRemove() {
        let time = new Date().getTime();
        if (this.lifeTime > 0 && time > this.removeAfter) {

            return true;
        }
        return false;
    }

    draw() {
        ctx.fillStyle = this.color;
        ctx.globalAlpha = this.transparency;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();
        ctx.stroke();
        ctx.globalAlpha = 1
    }
}

function generateParticleCloud(x, y, color, number, maxSize, speed) {
    if (!maxSize) {
        maxSize = 30;
    }
    for (let i = 0; i < number; i++) {
        let p = new Particle();
        p.speed = Math.random() * 0.2 + speed;
        p.x = x + (Math.random() * maxSize)
        p.y = y + (Math.random() * maxSize);
        p.color = color;
        p.radius = Math.random() * maxSize;
        partilclesArray.push(p)
    }
}

//  buttons


// Animation loop
function animate() {
    if (gamePause) {
        return;
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(background, 0, 0, 626, 417, 0, 0, canvas.width, canvas.height);
    handleBubbles();
    player.update();
    player.draw();

    ctx.fillStyle = "Black";
    ctx.font = defaultFont;
    ctx.fillText("Score: " + score, 10, 50);
    ctx.fillText("Oxygen: " + player.oxygen, 540, 50);
    ctx.fillText("LEVEL " + level, 300, 50);
    gameFrame++;


    if (level * 10 < score) {
        ctx.font = '60px Arial';

        ctx.font = defaultFont;

        ++level;
    }
    requestAnimationFrame(animate);

}





function startGame(el) {
    el.remove();
    el = document.getElementById('menu');
    el.remove();
    animate();
}